# aws-lambda



## Getting started

Documentation

Your Senior Management is concerned about engineers creating S3 buckets with public access and uploading unencrypted objects into S3 buckets. They want you to track situations like this and if you find any, they want to remediate them using Lambda. After the remediation is complete, they want you to send a notification to the security department about which buckets were remediated. You determine what your Lambda function should do.

Your solution should:

    Detect any S3 Buckets with public access

    Remediate the bucket(s) and block public access.

    Send an SNS notification of which bucket(s) were remediated.

**Solution Architechture Diagram**

AWS_S3Bucket_Remediation-S3Bucket_Remediation_Architecture.drawio

**Flow Chart Diagram For Lamda Remediation Process**

AWS_S3Bucket_Remediation-S3Remediation_FlowChart.drawio

**Project Execution Steps**

    Create an AWS Config rule to monitor s3 bucket compliance

    Create an SNS Topic and Subscription

    Create a role for Lambda

    Create an event bridge rule

    Create a Lamda function

    Verify your solution works

    Create aws config with rule checking for aws s3 bucket with public access

aws_config_rule

specify_rule_type

Choose_s3-bucket-level-public-access-prohibited

Click_add_rule

    Create an SNS topic and subscription specifying email as the protocol

    Create a policy (with any name of your choosing)for your lambda function with the following code

` “Version”: “2012-10-17”,

"Statement": [
    {
        "Sid": "VisualEditor0",
        "Effect": "Allow",
        "Action": [
            "config:GetComplianceDetailsByConfigRule",
            "lambda:*",
            "s3:GetBucketPolicy",
            "s3:PutBucketPolicy",
            "s3:PutObject",
            "s3:PutObjectAcl",
            "s3:*",
            "s3-object-lambda:*"
        ],
        "Resource": "*"
    }
]

}`

    Create a role, add AWSLambdaBasicExecutionRole policy and the custom policy you created in previous steps

    Create an event bridge rule, choose custom pattern and paste the following code in the **Event Pattern** section

`{

"source": [
  "aws.config"
],
"detail": {
  "requestParameters": {
    "evaluations": {
      "complianceType": [
        "NON_COMPLIANT"
      ]
    }
  },
  "additionalEventData": {
    "managedRuleIdentifier": [       
      "S3_BUCKET_LEVEL_PUBLIC_ACCESS_PROHIBITED"
    ]
  }
}

}`

    copy the Lamda function in your project and execute

    Create non-compliant S3 buckets and test your solution.

you should get an email of remediated buckets




## Authors and acknowledgment
David Edokpayi.

## License
For open source projects, say how it is licensed.

## Project status


